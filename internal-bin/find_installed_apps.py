#!/usr/bin/env python3
from shutil import which
import shlex
import argparse
import os
import sys
from typing import Any, Optional
# local files
from my_utils import load_yaml_with_overrides
# pip install yaml
import yaml

LIQUID_UNRESOLVED_VARIABLE_START = "{{"


def resolve_command(categories: list[str], config: Any) -> Optional[str]:
    if type(config) == dict:
        for cat in categories:
            cat_list = config.get(cat)
            if cat_list:
                match = resolve_command_from_list(cat_list)
                if match:
                    return match
        return None
    elif type(config) == list:
        return resolve_command_from_list(config)
    else:
        raise ValueError(f"Config has bad structure: {config}")


def parse_binary_from_command(command_line: str):
    # Remove arguments
    command = shlex.split(command_line)[0]

    # Resolve special paths
    if command.startswith("~"):
        command = os.path.expanduser(command)

    return command


def resolve_command_from_list(program_choices: list[str]) -> Optional[str]:
    """Returns the index of the first program that is installed or -1 if none of them match"""
    for index, command in enumerate(program_choices):
        program = parse_binary_from_command(command)
        if program.startswith(LIQUID_UNRESOLVED_VARIABLE_START) or which(program):
            return program_choices[index]
    return None


def parse_args() -> Any:
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="input file in YAML format")
    parser.add_argument("-c", "--categories", nargs="+", default=["default"], help="categories. Example: [i3, xorg, default]")
    parser.add_argument("-o", "--output", default="-", help="the file to write the results to. Use '-' for stdout")
    parser.add_argument("-p", "--program", help="checks for given program. Will write program to stdout. If the targeted program is not installed, it will return an error code.")
    return parser.parse_args()


def resolve_single_program(target_program: str, full_config: dict[str, Any], categories: list[str]) -> int:
    config = full_config.get(target_program)
    result = None
    if config:
        result = resolve_command(categories, config)
    
    if result:
        print(result)
        return 0
    else:
        print(f"No program installed for category '{target_program}'")
        return 1


def resolve_all_programs(output: str, full_config: dict[str, Any], categories: list[str]) -> int:
    results = {}
    for program, config in full_config.items():
        config = full_config.get(program)
        match = resolve_command(categories, config)
        if match:
            results[program] = match
    
    # Output all programs
    if output == "-":
        print(yaml.dump(results))
    else:
        output_stream = open(output, "w")
        yaml.dump(results, output_stream)
    return 0


def main() -> int:
    args = parse_args()

    path = args.input_file
    full_config = load_yaml_with_overrides(path)

    if args.program:
        return resolve_single_program(args.program, full_config, args.categories)
    else:
        return resolve_all_programs(args.output, full_config, args.categories)


if __name__ == "__main__":
    code = main()
    sys.exit(code)
