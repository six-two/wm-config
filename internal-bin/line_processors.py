#!/usr/bin/env python3
import shlex
# import local files
from config_parser import LineType, Bindsym, Exec, Line

FLAG_WMC_NO_WRAP = "--wmc-no-wrap"
I3_INCOMPATIBLE_BINDSYM_FLAGS = ["--locked"]
I3_KEY_NAME_SUBSTITUTIONS = {
    # Make sure space is always in lower case
    "space": "space",
    "alt": "Mod1",
}


def line_make_i3_compatible(line: Line) -> Line:
    if line.line_type == LineType.BINDSYM:
        assert isinstance(line.parsed, Bindsym)
        flags = [x for x in line.parsed.flags if x not in I3_INCOMPATIBLE_BINDSYM_FLAGS]
        # replace all key names via I3_KEY_NAME_SUBSTITUTIONS
        trigger_keys = [I3_KEY_NAME_SUBSTITUTIONS.get(x.lower(), x)
                        for x in line.parsed.trigger_keys]

        parsed = line.parsed._replace(flags=flags, trigger_keys=trigger_keys)
        return line._replace(parsed=parsed)
    else:
        return line


def line_wrap_exec(line: Line, exec_wrapper_command: str) -> Line:
    if line.line_type == LineType.BINDSYM:
        assert isinstance(line.parsed, Bindsym)
        parsed: Bindsym = line.parsed
        # only do it if there is an exec (and not an internal command)
        if parsed.exec_list:
            flags = parsed.exec_list[1:]
            new_flags = [x for x in flags if x != FLAG_WMC_NO_WRAP]
            removed_flag = len(new_flags) != len(flags)
            if removed_flag:
                # remove the nowrap flag
                new_exec_list = parsed.exec_list[:1] + new_flags
                parsed = parsed._replace(exec_list=new_exec_list)
            elif exec_wrapper_command:
                # wrap the exec
                escaped_command = escape_command(parsed.command)
                parsed = parsed._replace(command=f"{exec_wrapper_command} {escaped_command}")
        return line._replace(parsed=parsed)
    elif line.line_type == LineType.EXEC:
        assert isinstance(line.parsed, Exec)
        parsed: Exec = line.parsed
        new_flags = [x for x in parsed.flags if x != FLAG_WMC_NO_WRAP]
        removed_flag = len(new_flags) != len(parsed.flags)
        if removed_flag:
            # remove the nowrap flag
            parsed = parsed._replace(flags=new_flags)
        elif exec_wrapper_command:
            # wrap the exec
            escaped_command = escape_command(parsed.command)
            parsed = parsed._replace(command=f"{exec_wrapper_command} {escaped_command}")
        return line._replace(parsed=parsed)
    else:
        return line


def line_beautify(line: Line) -> Line:
    if line.line_type == LineType.BINDSYM:
        assert isinstance(line.parsed, Bindsym)
        flags = sorted(set(line.parsed.flags))
        trigger_keys = line.parsed.trigger_keys
        # heuristic for sorting keys
        trigger_keys = sorted(trigger_keys[:-1]) + [trigger_keys[-1]]
        parsed_bindsym = line.parsed._replace(flags=flags, trigger_keys=trigger_keys)
        return line._replace(parsed=parsed_bindsym)
    elif line.line_type == LineType.EXEC:
        assert isinstance(line.parsed, Exec)
        flags = sorted(set(line.parsed.flags))
        parsed_exec = line.parsed._replace(flags=flags)
        return line._replace(parsed=parsed_exec)
    else:
        return line


def escape_command(cmd: str) -> str:
    if cmd[0] == cmd[-1] and cmd[0] in ['"', '\'']:
        # this is not foolproof, but if you quote it, I will assume that you do it right
        # Bad example: "this line is" " badly quoted and maybe only the first part will be executed?"
        return cmd
    else:
        return shlex.quote(cmd)


def line_expand_key_variables(line: Line, key_variables: dict[str, list[str]]) -> list[Line]:
    if line.line_type == LineType.BINDSYM:
        assert isinstance(line.parsed, Bindsym)
        bindsym: Bindsym = line.parsed
        for i, key in enumerate(bindsym.trigger_keys):
            if key.startswith("@"):
                values = key_variables.get(key[1:])
                if values:
                    result = []
                    for v in values:
                        trigger_keys = bindsym.trigger_keys.copy()
                        trigger_keys[i] = v
                        modified_bindsym = bindsym._replace(trigger_keys=trigger_keys)
                        modified_line = line._replace(parsed=modified_bindsym)
                        # RECURSION!
                        result += line_expand_key_variables(modified_line, key_variables)
                    return result
                else:
                    raise Exception(f"No values for key variable: {key}")
    # Handle all other cases
    return [line]
