#!/usr/bin/env python3
import argparse
import os
import subprocess
# pip install setuptools
import pkg_resources


def test_requirements(path) -> bool:
    """Test that each required package is available."""
    # Ref: https://stackoverflow.com/a/45474387/
    requirements = pkg_resources.parse_requirements(open(path, "r"))
    ok = True
    for requirement in requirements:
        requirement = str(requirement)
        try:
            pkg_resources.require(requirement)
        except Exception:
            # print(f"Could not satisfy dependency '{requirement}'")
            ok = False
    return ok


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("pip_requirements_file")
    args = ap.parse_args()

    requirements_path = args.pip_requirements_file
    requirements_path = os.path.realpath(requirements_path)
    requirements_path = os.path.abspath(requirements_path)

    if test_requirements(requirements_path):
        print("Pip dependencies are already installed")
    else:
        print("Installing pip dependencies...")
        subprocess.call(["python3", "-m", "pip", "install", "-r", requirements_path])


if __name__ == "__main__":
    main()
