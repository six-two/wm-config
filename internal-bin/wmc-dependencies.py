#!/usr/bin/env python3
import csv
import subprocess
from typing import NamedTuple
# local files
from my_utils import load_wmc_yaml_file, WMC, J

PRIORITY_NAMES = [None, "Required", "Recommended", "Useful", "Nice to have"]

class CsvRow(NamedTuple):
    package_name: str
    priority: int
    wm_list: set[str]
    in_vm: bool
    description: str

def get_installed_packages() -> list[str]:
    """
    This script assumes, that you are using Arch Linux
    """
    output_bytes = subprocess.check_output(["pacman", "-Q"])
    output_lines = output_bytes.decode("utf-8").split("\n")
    # example line: "zstd 1.5.0-1"
    # Get only the first element (the package name)
    package_names = [x.strip().split()[0] for x in output_lines if x.strip()]
    return package_names

def parse_csv(path: str) -> list[CsvRow]:
    parsed = []
    with open(path, "r") as f:
        csvReader = csv.reader(f, delimiter=":")
        # Skip header
        next(csvReader)

        for row in csvReader:
            package_name = str(row[0])
            priority = int(row[1])
            wm_list = set(str(row[2]).split(","))
            in_vm = str(row[3]) == "y"
            description = str(row[4])

            parsed_row = CsvRow(package_name, priority, wm_list, in_vm, description)
            parsed.append(parsed_row)

    return parsed



def get_missing_rows(wanted_wm_list: set[str], csv_file_name: str, installed: set[str]) -> list[CsvRow]:
    path_csv = J(WMC, "dependencies", f"{csv_file_name}.csv")
    rows = parse_csv(path_csv)
    wanted_rows = [x for x in rows if x.wm_list.intersection(wanted_wm_list)]
    return [x for x in wanted_rows if x.package_name not in installed]

def print_missing_packages(csv_file_name: str, rows: list[CsvRow]) -> None:
    if rows:
        print(f" {csv_file_name} ".center(80, "="))
        print(f" {len(rows)} recommended package(s) are not installed ".center(80, "+"))
        for r in sorted(rows, key=lambda x: x.priority):
            priority = PRIORITY_NAMES[r.priority]
            text = f"[{priority}] {r.package_name}"
            if r.description:
                text += f": {r.description}"
            print(text)

if __name__ == "__main__":
    config = load_wmc_yaml_file()
    wm_list = set(config.wm_list)
    installed_packages = set(get_installed_packages())

    first_output = True
    for csv_name in ["pacman", "aur"]:
        rows = get_missing_rows(wm_list, csv_name, installed_packages)
        if rows:
            if not first_output:
                print()
            first_output = False
            print_missing_packages(csv_name, rows)
