# pylint: disable=inherit-non-class
import os
from typing import Any, NamedTuple
import sys
# pip install pyyaml
import yaml
# pip install jsonmerge
import jsonmerge

J = os.path.join
HOME = os.path.expanduser("~")
WMC = os.path.expanduser("~/.wmc")
CONFIG = os.path.expanduser("~/.config")
WMC_CONFIG = os.getenv("WMC_CONFIG_DIR") or J(WMC, "config")
WMC_TEMPLATE = os.getenv("WMC_TEMPLATE_DIR") or J(WMC, "template")
WINDOW_MANAGERS = ["i3", "sway"]


class WmcYaml(NamedTuple):
    wm_list: list[str]
    profile: str
    theme: str
    is_vm: bool
    mod: str
    exec_wrapper: str
    raw: dict[str, Any]


def load_yaml(path: str, allow_missing: bool = False) -> Any:
    if os.path.exists(path):
        with open(path, "rb") as byte_stream:
            return yaml.safe_load(byte_stream)
    elif not allow_missing:
        raise Exception(f"YAML file does not exist: '{path}'")
    else:
        return None


def load_yaml_with_overrides(path: str, deep_merge: bool = False) -> Any:
    if not path.endswith(".yaml"):
        raise Exception("Path should end in .yaml")
    # Replace ".yaml" with "-override.yaml"
    path_override = f"{path[:-5]}-override.yaml"
    return load_and_merge_configs(path, path_override, deep_merge)


def load_config_yaml(relative_config_base_name: str, deep_merge: bool = False) -> Any:
    path_defaults = J(WMC_CONFIG, relative_config_base_name + ".yaml")
    path_override = J(WMC_CONFIG, relative_config_base_name + "-override.yaml")
    return load_and_merge_configs(path_defaults, path_override, deep_merge)


def load_and_merge_configs(path_defaults: str, path_override: str, deep_merge: bool = False) -> Any:
    defaults = load_yaml(path_defaults)
    overrides = load_yaml(path_override, allow_missing=True)
    if overrides:
        if deep_merge:
            defaults = jsonmerge.merge(defaults, overrides)
        else:
            defaults.update(overrides)
    return defaults


def get_non_empty_field(obj: dict[str, Any], field_name: str) -> Any:
    value = obj.get(field_name)
    if value:
        return value
    else:
        raise Exception(f"Missing or empty field: '{field_name}'")


def load_wmc_yaml_file() -> WmcYaml:
    obj = load_config_yaml("wmc", deep_merge=True)
    wm_list = [str(x) for x in get_non_empty_field(obj, "window_managers")]
    for wm in wm_list:
        if wm not in WINDOW_MANAGERS:
            raise Exception(f"Unknown window manager: '{wm}'")

    profile = str(get_non_empty_field(obj, "profile"))
    theme = str(get_non_empty_field(obj, "theme"))
    exec_wrapper = obj.get("exec_wrapper", "")

    is_vm = obj.get("is_vm")
    if is_vm != True and is_vm != False:
        raise Exception(
            "Field 'is_vm' is not a boolean. Allowed values: true, false")

    mod = str(get_non_empty_field(obj, "mod"))
    if mod not in ["Mod1", "Mod4"]:
        print(
            f"[WARN]: Your modifier ({mod}) is neither the Alt key (Mod1) nor the Windows key (Mod4)")

    return WmcYaml(
        wm_list=wm_list,
        profile=profile,
        theme=theme,
        is_vm=bool(is_vm),
        mod=mod,
        exec_wrapper=exec_wrapper,
        raw=obj,
    )


def get_wm_categories(wm: str) -> list[str]:
    if wm == "i3":
        return ["i3", "xorg", "default"]
    elif wm == "sway":
        return ["sway", "wayland", "default"]
    else:
        print("[WARN] Unknown wm, could not decide if it is wayland or xorg based")
        return [wm, "default"]


def fatal_error(message: str):
    print(message)
    sys.exit(1)
