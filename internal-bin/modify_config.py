#!/usr/bin/env python3
import argparse
# import local files
from config_parser import LineType, Bindsym, Comment, Exec, Mode, Section, Line, parse_string
from process_liquid import OPTIONAL_FAILED
from line_processors import line_beautify, line_make_i3_compatible, line_wrap_exec, line_expand_key_variables
from my_utils import load_wmc_yaml_file


def parse_lines_from_file(path: str) -> list[Line]:
    with open(args.input, "rb") as f:
        input_text = f.read().decode("utf-8")
    return parse_string(input_text)


def line_to_text(line: Line, indent: int, remove_comments: bool) -> str:
    text = " " * indent
    if line.line_type == LineType.BINDSYM:
        assert isinstance(line.parsed, Bindsym)
        bindsym: Bindsym = line.parsed
        text += "bindsym"
        for flag in bindsym.flags:
            text += " " + flag
        text += " " + "+".join(bindsym.trigger_keys)
        for exec_or_flag in bindsym.exec_list:
            text += " " + exec_or_flag
        text += " " + bindsym.command
    elif line.line_type == LineType.COMMENT:
        assert isinstance(line.parsed, Comment)
        if remove_comments:
            text = ""
        else:
            comment: Comment = line.parsed
            text += comment.comment
    elif line.line_type == LineType.EMPTY:
        text = ""
    elif line.line_type == LineType.EXEC:
        assert isinstance(line.parsed, Exec)
        exec_line: Exec = line.parsed
        text += "exec"
        # flags, command
        for flag in exec_line.flags:
            text += " " + flag
        text += " " + exec_line.command
    elif line.line_type == LineType.MODE_OR_SECTION_END:
        text += "}"
    elif line.line_type == LineType.MODE_START:
        assert isinstance(line.parsed, Mode)
        mode: Mode = line.parsed
        text += f"mode {mode.name} {{"
    elif line.line_type == LineType.SECTION_START:
        assert isinstance(line.parsed, Section)
        section: Section = line.parsed
        text += f"{section.name} {{"
    elif line.line_type == LineType.UNKNOWN:
        text += line.raw.lstrip()
    else:
        raise Exception(f"Unknown line type: {line.line_type}")

    return text


def lines_to_text(lines: list[Line], remove_comments: bool) -> str:
    text = ""
    blocks: list[str] = []
    for index, line in enumerate(lines):
        # Decrease the indent for the section/mode end
        if line.line_type == LineType.MODE_OR_SECTION_END:
            if not blocks:
                raise Exception(f"Error in line {index + 1} ('{line.raw}'): Not in a block")
            blocks = blocks[:-1]

        # Add a new line in front of every mode
        if line.line_type == LineType.MODE_START:
            text += "\n"

        # Remove empty lines
        indent = 4 * len(blocks)
        line_str = line_to_text(line, indent, remove_comments)
        if line_str:
            text += line_str + "\n"

        # Increase the indent after a section/mode start
        if line.line_type == LineType.SECTION_START:
            assert isinstance(line.parsed, Section)
            blocks.append(line.parsed.name)
        elif line.line_type == LineType.MODE_START:
            if blocks:
                raise Exception(f"Error in line {index + 1} ('{line.raw}'): Already in one or more blocks: " + ", ".join(blocks))
            assert isinstance(line.parsed, Mode)
            blocks.append(line.parsed.name)

    if blocks:
        raise Exception("Unclosed blocks: " + ", ".join(blocks))
    return text


def process_line(line: Line, args) -> Line:
    if args.i3:
        line = line_make_i3_compatible(line)
    line = line_wrap_exec(line, args.exec_wrapper)
    line = line_beautify(line)
    return line


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Beautify a config file and make small changes to it")
    parser.add_argument("input", help="the input file")
    parser.add_argument("-o", "--output", help="write output to a file")
    parser.add_argument("-e", "--exec-wrapper", help="all exec commands will call the wrapper script which will receive the command as an argument")
    parser.add_argument("-c", "--remove-comments", action="store_true", help="remove comment lines")
    parser.add_argument("-3", "--i3", action="store_true", help="try to make the config i3 compatible")
    parser.add_argument("-d", "--debug", action="store_true", help="prints additional debug infos")
    args = parser.parse_args()

    if args.debug:
        print("Args:", args)

    local_vars = load_wmc_yaml_file().raw.get("settings", {})
    
    orginal_lines = parse_lines_from_file(args.input)

    # remove lines, where the 'optional' filter failed
    lines = [x for x in orginal_lines if OPTIONAL_FAILED not in x.raw]
    if args.debug:
        count_removed = len(orginal_lines) - len(lines)
        print(f"Removed {count_removed} line(s) containing '{OPTIONAL_FAILED}'")

    key_vars = local_vars.get("keys", {})
    
    expanded_lines = []
    for line in lines:
        expanded_lines += line_expand_key_variables(line, key_vars)
    lines = expanded_lines

    lines = [process_line(x, args) for x in lines]

    output = lines_to_text(lines, args.remove_comments)
    if args.output:
        with open(args.output, "wb") as f:
            f.write(output.encode("utf-8"))
    else:
        print(output)

