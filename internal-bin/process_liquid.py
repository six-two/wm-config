#!/usr/bin/env python3
import argparse
import functools
import os
import subprocess
import sys
import traceback
from typing import Any
# pip install python-liquid, you may need to uninstall liquidpy
from liquid import Environment, FileSystemLoader
# pip install munch
from munch import munchify
# pip install pyyaml
import yaml

DEBUG = True
OPTIONAL_FAILED = "#REMOVE_THIS_LINE#"


def exit_on_error(error_message: str):
    def wrapper(func):
        @functools.wraps(func)
        def call(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except SystemExit:
                return
            except:
                exit_because_fatal_error(error_message, DEBUG)
        return call
    return wrapper


def exit_because_fatal_error(message: str, debug: bool):
    print("Fatal error:", message)
    if debug:
        traceback.print_exc()
    else:
        print("Hint: Run this script with '-d' or '--debug' to see a stacktrace")
    sys.exit(1)


def read_file(path: str) -> bytes:
    try:
        with open(path, "rb") as f:
            return f.read()
    except:
        exit_because_fatal_error(f"Failed to read file '{path}'", DEBUG)
        raise Exception("This statement should never be executed")


@exit_on_error("Failed to write to output file")
def write_output_file(path: str, contents: str) -> None:
    with open(path, "wb") as f:
        f.write(contents.encode("utf-8"))


@exit_on_error("Failed to parse the YAML file containing the variables")
def parse_vars_file(path: str) -> Any:
    file_bytes = read_file(path)
    variables = yaml.safe_load(file_bytes)
    return munchify(variables)


# @exit_on_error("Failed to parse arguments")
def parse_arguments() -> Any:
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="the input file(s) and/or folder(s)")
    parser.add_argument("-d", "--debug", action="store_true", help="prints additional debug infos")
    parser.add_argument("-i", "--include-dir", help="base path to use for includes. Defaults to the folder that the input file is in.")
    parser.add_argument("-o", "--output", help="write output to a file")
    parser.add_argument("-r", "--recursive", action="store_true", help="process the output via multiple iterations, until it is not changing anymore")
    parser.add_argument("-v", "--vars", required=True, help="a yaml file containing the values for the liquid templates")
    try:
        args = parser.parse_args()
    except:
        # print("Failed to parse arguments")
        sys.exit(1)

    global DEBUG
    DEBUG = args.debug

    if DEBUG:
        print("Parsed arguments: ", args)

    return args


@exit_on_error("Internal error in filter 'optional'")
def filter_optional(argument: Any) -> str:
    # Return arg if it evailuates to a non empty scring, otherwise return a special string
    if argument and str(argument).strip():
        return str(argument)
    else:
        return OPTIONAL_FAILED


def filter_require(argument: Any) -> Any:
    if argument:
        return argument
    else:
        raise Exception("Filter 'require': Value does not exist")


def filter_chain_cmd(argument: Any) -> Any:
    if argument:
        argument = f" | true && {argument}"
    return argument

def filter_create_keybindings(argument: Any) -> str:
    template = []
    LOOP_ARGS = [
        ("keys", ""), # normal keys
        ("mod_keys", "{{mod}}+"), # mod keys
        ("mod_ctrl_keys", "{{mod}}+Ctrl+"), # ctrl and mod keys
        ("mod_alt_keys", "{{mod}}+Alt+"), # alt and mod keys pressed
    ]
    for var_name, modifiers in LOOP_ARGS:
        template_str = "{% for key_binding in " + str(argument) + "." + var_name + " %}\n"
        template_str += "bindsym " + modifiers + "{{ key_binding.trigger }} {{ key_binding.command | optional }}\n"
        template_str += "{% endfor %}"
        template.append(template_str)

    # print(template)
    # subprocess.call(["notify-send", "create_keybindings", "\n\n".join(template )])
    return "\n\n".join(template)


@exit_on_error("Failed to process the liquid template")
def process_liquid(template: str, variables: Any, include_dir: str) -> str:
    liquid_env = Environment(
        globals=variables,
        loader=FileSystemLoader(include_dir),
    )
    liquid_env.add_filter("opt", filter_optional)
    liquid_env.add_filter("optional", filter_optional)
    liquid_env.add_filter("req", filter_require)
    liquid_env.add_filter("require", filter_require)
    liquid_env.add_filter("chain_cmd", filter_chain_cmd)
    liquid_env.add_filter("create_keybindings", filter_create_keybindings)

    bound_template = liquid_env.from_string(template)
    return bound_template.render()

def process_liquid_recursive(template: str, variables: Any, include_dir: str, max_iterations: int = 10) -> str:
    for i in range(max_iterations):
        last_template = template
        template = process_liquid(last_template, variables, include_dir)

        # No changes in the template -> no liquid left -> we are finished
        if template == last_template:
            return template

        if DEBUG:
            print(f"Iteration {i+1}: {len(last_template)} -> {len(template)} bytes")
    
    write_output_file("/tmp/old_template.txt", last_template)
    write_output_file("/tmp/new_template.txt", template)

    print(f"Fatal error: Template still changing after {max_iterations} iterations.")
    print("Exiting to prevent a endless loop.")

    print("==== Running `diff` on the templates =====")
    subprocess.call(["diff", "/tmp/old_template.txt", "/tmp/new_template.txt"])
    sys.exit(1)


@exit_on_error("Internal error")
def main():
    args = parse_arguments()

    # Get variables from args
    output_path = args.output
    liquid_template_path = args.input
    liquid_variables_path = args.vars
    liquid_include_dir = args.include_dir

    # Default to parent of input file
    if not liquid_include_dir:
        liquid_include_dir = os.path.dirname(liquid_template_path)

    if DEBUG:
        print("Parsed arguments: ", args)

    liquid_template = read_file(liquid_template_path).decode("utf-8")
    liquid_variables = parse_vars_file(liquid_variables_path)
    if DEBUG:
        print("Liquid variables:", liquid_variables)

    process_function = process_liquid_recursive if args.recursive else process_liquid
    output = process_function(liquid_template, liquid_variables, liquid_include_dir)
    if output_path:
        write_output_file(output_path, output)
    else:
        print(output)


if __name__ == "__main__":
    main()
    
