#!/usr/bin/env python3
import argparse
import os
import shlex
import subprocess
import sys
# local
from my_utils import J, WMC, HOME, WINDOW_MANAGERS, WmcYaml, load_wmc_yaml_file

WARN = "WARN"
OK = "ok"
ERROR = "error"
MARKER = "#"

# Try to color the status text
try:
    # pip install py-term
    import term
    WARN = term.format(WARN, term.yellow, term.bold)
    OK = term.format(OK, term.green, term.bold)
    ERROR = term.format(ERROR, term.red, term.bold)
    MARKER = term.format(MARKER, term.yellow, term.bold)
except:
    pass


def run_build_statusbar(wmc_yaml: WmcYaml) -> bool:
    preprocessor_name = "vm" if wmc_yaml.is_vm else "default"
    command = [J(WMC, "internal-bin", "build-statusbar.sh"), preprocessor_name]
    return run_process(command, "Building statusbar")


def run_register_my_bins(wmc_yaml: WmcYaml) -> bool:
    flags = wmc_yaml.raw["settings"]["my_bins_install_flags"].split()
    print("[D] Enabling binaries with flags:", " ".join(flags))
    command = [J(WMC, "repos", "my-bins", "install.sh"), *flags]
    return run_process(command, "Installing my 'bin' repository")

def run_build_wm_config(wm: str, debug: bool) -> bool:
    command = [J(WMC, "internal-bin", "build-wm-config.py")]
    if wm_list:
        command += ["--window-manager", wm]
    if debug:
        command += ["--debug"]
    return run_process(command, f"Building {wm} config")


def run_build_bins(wm: str, debug: bool) -> bool:
    command = [J(WMC, "internal-bin", "build-bins.py")]
    command += [J(WMC, "config", "bin.yaml")]
    command += [J(HOME, ".config", wm, "bin")]
    if wm == "i3":
        command += ["-c", "i3", "xorg", "default"]
    elif wm == "sway":
        command += ["-c", "sway", "wayland", "default"]
    return run_process(command, f"Building {wm} bins")


def run_reload_wm(wm: str) -> bool:
    if wm == "i3":
        return run_process([wm, "restart"], f"Restarting {wm}")
    elif wm == "sway":
        return run_process([wm, "reload"], f"Reloading {wm}")
    else:
        print(f"Unknown wm: '{wm}'")
        return False


def run_process(command: list[str], message: str) -> bool:
    print(f"[{MARKER}] {message}... ", end="", flush=True)
    try:
        # Hide the output by default
        subprocess.check_output(command, stderr=subprocess.STDOUT)
        print(OK)
        return True
    except subprocess.CalledProcessError as ex:
        print(ERROR)
        # show the output when requested
        print(ex.output.decode("utf-8"))
        return False


def run_post_build_hook() -> bool:
    hook_file = J(WMC, "config", "post_build.sh")
    if os.path.exists(hook_file):
        return run_process([hook_file], "Running post build hook")
    else:
        return True # No hook is also allowed, so we claim we are successful


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--window-managers", nargs="+", choices=WINDOW_MANAGERS, help="only update files for these window managers")
    parser.add_argument("-S", "--no-statusbar", action="store_true", help="do not build the statusbar config")
    parser.add_argument("-C", "--no-wm-configs", action="store_true", help="do not build the window manager configs")
    parser.add_argument("-R", "--no-reload", action="store_true", help="do not reload the window manager")
    parser.add_argument("-d", "--debug", action="store_true", help="enable additional debugging outputs")
    args = parser.parse_args()

    debug = args.debug
    if debug:
        print("Args:", args)

    wmc_yaml = load_wmc_yaml_file()

    build_statusbar = not args.no_statusbar
    build_wm_configs = not args.no_wm_configs
    wm_list = args.window_managers or wmc_yaml.wm_list
    reload_wm = not args.no_reload
    ok = True

    for wm in wm_list:
        output_dir = J(HOME, ".config", wm)
        os.makedirs(output_dir, exist_ok=True)

    ok &= run_build_statusbar(wmc_yaml)
    ok &= run_register_my_bins(wmc_yaml)

    if build_wm_configs:
        for wm in wm_list:
            ok &= run_build_wm_config(wm, debug)

    for wm in wm_list:
        ok &= run_build_bins(wm, debug)


    if ok:
        # Only run the post build hook if everything else succeded. This is because running the script twice may break the config (users won't program it defensively)
        ok &= run_post_build_hook()

    if (build_wm_configs or build_statusbar) and reload_wm:
        if ok:
            current_wm = os.getenv("XDG_CURRENT_DESKTOP")
            if current_wm and current_wm in ["i3", "sway"]:
                run_reload_wm(current_wm)
        else:
            print(f"[{WARN}] Cancelled reloading because of build errors")

    if not ok:
        sys.exit(2)
