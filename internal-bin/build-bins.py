#!/usr/bin/env python3
from shutil import which
import shlex
import shutil
import argparse
import os
import sys
from typing import Any, Optional
# local files
from my_utils import load_yaml_with_overrides


LIQUID_UNRESOLVED_VARIABLE_START = "{{"


def main() -> int:
    args = parse_args()

    full_config = load_yaml_with_overrides(args.input_file)

    # Clean up output folder
    out_dir = args.output_dir
    shutil.rmtree(out_dir, ignore_errors=True)
    os.makedirs(out_dir, exist_ok=True)

    for program, config in full_config.items():
        config = full_config.get(program)
        match = resolve_command(args.categories, config)
        if match:
            print("Name:", program)
            print("Command:", match)
            print()
    
            # TODO process liquid in command

            # Create a file that executes the command
            contents = f'#!/usr/bin/bash\n{match} "$@"'

            path = os.path.join(out_dir, f"wmc-{program}")
            with open(path, "w") as f:
                f.write(contents)
            os.chmod(path, 0x755)
    return 0


def resolve_command(categories: list[str], config: Any) -> Optional[str]:
    if type(config) == dict:
        for cat in categories:
            cat_list = config.get(cat)
            if cat_list:
                match = resolve_command_from_list(cat_list)
                if match:
                    return match
        return None
    elif type(config) == list:
        return resolve_command_from_list(config)
    else:
        raise ValueError(f"Config has bad structure: {config}")



def resolve_command_from_list(program_choices: list[str]) -> Optional[str]:
    """Returns the index of the first program that is installed or -1 if none of them match"""
    for index, command in enumerate(program_choices):
        program = parse_binary_from_command(command)
        if program.startswith(LIQUID_UNRESOLVED_VARIABLE_START) or which(program):
            return program_choices[index]
    return None


def parse_binary_from_command(command_line: str):
    # Remove arguments
    command = shlex.split(command_line)[0]

    # Resolve special paths
    if command.startswith("~"):
        command = os.path.expanduser(command)

    return command


def parse_args() -> Any:
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="input file in YAML format")
    parser.add_argument("output_dir", help="the directory to write the results to.")
    parser.add_argument("-c", "--categories", nargs="+", default=["default"], help="categories. Example: [i3, xorg, default]")
    return parser.parse_args()


if __name__ == "__main__":
    code = main()
    sys.exit(code)

