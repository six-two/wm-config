#!/usr/bin/bash
if [[ $# -ne 1 ]]; then
  echo "Usage: [preprocessor-name]"
  exit 1
fi

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
REPO="${SCRIPT_DIR}/../repos/statusbar"

# Just delegate this to the statusbar's build script
"${REPO}/build_and_apply_config.sh" "$1"
