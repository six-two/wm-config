#!/usr/bin/env python3
# pylint: disable=inherit-non-class
import argparse
import json
from typing import NamedTuple
import os
import subprocess
import shutil
import shlex
import sys
from tempfile import TemporaryDirectory
# pip install pyyaml
import yaml
# local files
from my_utils import J, WMC, WMC_CONFIG, WMC_TEMPLATE, HOME, WmcYaml, load_yaml, load_config_yaml, load_and_merge_configs, load_wmc_yaml_file, get_wm_categories, WINDOW_MANAGERS
from process_liquid import OPTIONAL_FAILED

class Config(NamedTuple):
    wmc: WmcYaml
    wm: str
    debug: bool
    output_file: str
    tmp: str


def generate_wm_config(config: Config) -> None:
    tmp_dir = config.tmp
    var_file = J(tmp_dir, "liquid_vars.yaml")
    config_intermediate = J(tmp_dir, "config-intermediate")
    config_final = J(tmp_dir, "config")
    env_file = J(tmp_dir, "env")

    generate_vars_file(var_file, config)

    # generate env file
    process_env_liquid(config, var_file, env_file)

    # generate wm config
    process_wm_liquid(config, var_file, config_intermediate)
    modify_config(config, config_intermediate, config_final)
    validate_config(config, config_final)

    # move files over
    move_config_to_location(config)


def load_keybindings_yaml_file(name: str) -> dict:
    launcher = load_config_yaml(name, deep_merge=True)
    for field_name in ["keys", "mod_keys", "mod_ctrl_keys", "mod_alt_keys"]:
        keys = []
        for key, command in launcher.get(field_name, {}).items():
            # Only check if the first character is upper or lower case
            is_lower = key[0] == key[0].lower()
            # Potential bug: we assume that every key name starts capitalized. If this fails i need to add a list of keys to start with a lower case character
            # Title case may not fit, since it would convert some following characters to lower case
            key = key[0].upper() + key[1:]

            key_combo = key if is_lower else f"Shift+{key}"
            keys.append({
                "trigger": key_combo,
                "command": command,
            })
        launcher[field_name] = keys
    return launcher


def load_theme(config: Config) -> dict:
    theme_default = J(WMC, "themes", "default", "theme.yaml")
    theme_current = J(WMC, "themes", config.wmc.theme, "theme.yaml")
    if not os.path.exists(theme_current):
        print(f"Fatal error: Unknown theme '{config.wmc.theme}'")
        sys.exit(1)
    theme = load_and_merge_configs(theme_default, theme_current)
    return theme


def generate_vars_file(path: str, config: Config) -> None:
    local_vars = config.wmc.raw.get("settings")
    if not local_vars:
        raise Exception(f"No 'settings' in object: {config.wmc.raw}")

    workspaces = local_vars["workspaces"].split(",")
    local_vars["workspaces"] = [
        {
            "key": str(key_name),
            "name": f"{index+1}:{key_name}",
        } for index, key_name in enumerate(workspaces)
    ]

    is_vm = bool(config.wmc.is_vm)
    data = {
        # Host / VM variables
        "is_host": not is_vm,
        "is_vm": is_vm,
        "host": OPTIONAL_FAILED if is_vm else "",
        "vm": "" if is_vm else OPTIONAL_FAILED,
        # different config files
        "apps": get_available_apps(config),
        "config": local_vars,
        "launcher": load_keybindings_yaml_file("launcher"),
        "mode_default": load_keybindings_yaml_file("mode_default"),
        "theme": load_theme(config),
        # variables
        "mod": config.wmc.mod,
        "wm": config.wm,
    }

    with open(path, "w") as out_stream:
        yaml.dump(data, out_stream)


def process_wm_liquid(config: Config, var_file: str, output_file: str):
    # assemble the command to build the wm config file
    command = [J(WMC, "internal-bin", "process_liquid.py")]
    command += [J(WMC_TEMPLATE, "main.conf")]
    command += ["--output", output_file]
    command += ["--include-dir", WMC_TEMPLATE]
    command += ["--recursive"]
    command += ["--vars", var_file]
    if config.debug:
        command += ["--debug"]

    if subprocess.call(command) != 0:
        raise Exception("Failed to run command: " + shlex.join(command))


def process_env_liquid(config: Config, var_file: str, output_file: str):
    # assemble the command to build the wm config file
    command = [J(WMC, "internal-bin", "process_liquid.py")]
    command += [J(WMC, "env", "main.sh")]
    command += ["--output", output_file]
    command += ["--include-dir", J(WMC, "env")]
    command += ["--recursive"]
    command += ["--vars", var_file]
    if config.debug:
        command += ["--debug"]

    if subprocess.call(command) != 0:
        raise Exception("Failed to run command: " + shlex.join(command))



def modify_config(config: Config, input_path: str, output_path: str):
    command = [J(WMC, "internal-bin", "modify_config.py")]
    command += [input_path]
    command += ["--output", output_path]
    command += ["--remove-comments"]

    if config.wmc.exec_wrapper:
        command += ["--exec-wrapper", config.wmc.exec_wrapper]

    if config.debug:
        command += ["--debug"]

    if config.wm == "i3":
        command += ["--i3"]

    if subprocess.call(command) != 0:
        raise Exception("Failed to run command: " + shlex.join(command))


def validate_config(config: Config, path: str):
    if config.wm == "i3":
        validation_command = ["i3", "-c", path, "-C"]
    elif config.wm == "sway":
        validation_command = ["sway", "-c", path, "--validate"]
    else:
        raise Exception(f"Unknown window manager: '{config.wm}'")
    if subprocess.call(validation_command) != 0:
        raise Exception("The generated config file failed validation")


def move_config_to_location(config: Config):
    wm_dir = J(HOME, ".config", config.wm)
    if not os.path.exists(wm_dir):
        os.mkdir(wm_dir)

    for file_name in ["config", "apps.yaml", "env"]:
        src = J(config.tmp, file_name)
        dest = J(HOME, ".config", config.wm, file_name)
        if os.path.exists(dest):
            os.remove(dest)
        shutil.copy(src, dest)

    # Convert variables to JSON for easy parsing (with jq)
    with open(J(config.tmp, "liquid_vars.yaml")) as input_file:
        data = yaml.safe_load(input_file)
    # @TODO: replace liquid in values (like apps, etc)
    with open(J(HOME, ".config", config.wm, "vars.json"), "w") as output_file:
        json.dump(data, output_file)


def get_available_apps(config: Config) -> dict[str, str]:
    tmp_file = J(config.tmp, "apps.yaml")
    command = [J(WMC, "internal-bin", "find_installed_apps.py")]
    command += [J(WMC_CONFIG, "apps.yaml")]
    command += ["--output", tmp_file]
    command += ["--categories", *get_wm_categories(config.wm)]

    if subprocess.call(command) != 0:
        raise Exception("Failed to run command: " + shlex.join(command))

    return load_yaml(tmp_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--window-manager", required=True, choices=WINDOW_MANAGERS, help="the window manager to build the config for")
    parser.add_argument("-o", "--output", help="the file to write the results to")
    parser.add_argument("-d", "--debug", action="store_true", help="enable additional debugging outputs")
    args = parser.parse_args()

    debug = args.debug
    if debug:
        print("Args:", args)

    wm = args.window_manager
    output_file = args.output or J(HOME, ".config", wm, "config")

    config = Config(
        wmc=load_wmc_yaml_file(),
        wm=wm,
        debug=debug,
        output_file=output_file,
        tmp="/tmp/TODO",
    )
    if debug:
        tmp_dir = "/tmp/wmc"
        shutil.rmtree(tmp_dir, ignore_errors=True)
        os.mkdir(tmp_dir)
        config = config._replace(tmp=tmp_dir)
        generate_wm_config(config)
    else:
        with TemporaryDirectory() as tmp_dir:
            config = config._replace(tmp=tmp_dir)
            generate_wm_config(config)
