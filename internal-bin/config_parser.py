#!/usr/bin/env python3
# pylint: disable=inherit-non-class
from typing import NamedTuple, Union
from enum import Enum, auto
import re


class LineType(Enum):
    BINDSYM = auto()
    COMMENT = auto()
    EMPTY = auto()
    EXEC = auto()
    MODE_OR_SECTION_END = auto()
    MODE_START = auto()
    SECTION_START = auto()
    UNKNOWN = auto()


class Bindsym(NamedTuple):
    flags: list[str]
    trigger_keys: list[str]
    exec_list: list[str]
    command: str


class Comment(NamedTuple):
    comment: str


class Exec(NamedTuple):
    flags: list[str]
    command: str


class Mode(NamedTuple):
    name: str


class Section(NamedTuple):
    name: str


class Line(NamedTuple):
    raw: str
    line_type: LineType
    # pylint: disable=unsubscriptable-object
    parsed: Union[Bindsym, Comment, Exec, Mode, Section, None]


def line_regex(regex: str):
    """
    This function converts a easier to read and understand regex into a better one
    """
    # replace spaces with whitespace regexes
    # double space -> at least one whitespace
    regex = regex.replace("  ", r"\s+")
    regex = regex.replace(" ", r"\s*")  # single space -> optional whitespace
    # make sure it starts with a ^
    if not regex.startswith("^"):
        regex = "^" + regex
    # make sure it ends with a $
    if not regex.endswith("$"):
        regex += "$"
    # now compile the regex
    return re.compile(regex)


OPTIONAL_FLAGS = r"((?:--\S+  )*)"
REGEX_BINDSYM = line_regex(r"bindsym  " + OPTIONAL_FLAGS + r"([^-\s]\S*)  (\S.*)")
REGEX_EXEC = line_regex(r"exec  " + OPTIONAL_FLAGS + r"([^-\s].*)")
REGEX_MODE = line_regex(r"mode  (\S(.*\S)?) {")
REGEX_SECTION_START = line_regex(r"(\w+) {")


def parse_string(config_text: str) -> list[Line]:
    return [parse_line(x) for x in config_text.split("\n")]


def assert_re_match_groups(regex: re.Pattern, line: str) -> list[str]:
    match = regex.match(line)
    if match:
        return [str(x) for x in match.groups()]
    else:
        raise Exception(f"Failed to apply regex '{regex}' to line '{line}'")


def split_flags(flags_str: str) -> list[str]:
    flag_name_list = [x.strip() for x in flags_str.split("--")]
    return [f"--{flag_name}" for flag_name in flag_name_list if flag_name]


def parse_line(line: str) -> Line:
    raw = line
    line = line.strip()

    line_type: LineType = LineType.EMPTY
    # pylint: disable=unsubscriptable-object
    parsed: Union[Bindsym, Comment, Exec, Mode, Section, None] = None
    if line:
        first_word = line.split()[0]
        if first_word.startswith("#"):
            line_type = LineType.COMMENT
            parsed = Comment(line)
        elif first_word == "bindsym":
            line_type = LineType.BINDSYM
            flags_str, key_combo, action = assert_re_match_groups(REGEX_BINDSYM, line)
            flags = split_flags(flags_str)
            trigger_keys = key_combo.split("+")
            action = action.strip()
            if action.split()[0] == "exec":
                exec_flags_str, command = assert_re_match_groups(REGEX_EXEC, action)
                exec_flags = split_flags(exec_flags_str)
                exec_list = ["exec"] + exec_flags
            else:
                command = action
                exec_list = []
            parsed = Bindsym(flags=flags, trigger_keys=trigger_keys, exec_list=exec_list, command=command)
        elif first_word == "exec":
            line_type = LineType.EXEC
            flags_str, command = assert_re_match_groups(REGEX_EXEC, line)
            flags = split_flags(flags_str)
            parsed = Exec(flags=flags, command=command)
        elif first_word == "mode":
            line_type = LineType.MODE_START
            name = assert_re_match_groups(REGEX_MODE, line)[0]
            parsed = Mode(name=name)
        elif first_word == "}":
            line_type = LineType.MODE_OR_SECTION_END
            parsed = None
        else:
            match = REGEX_SECTION_START.match(line)
            if match:
                line_type = LineType.SECTION_START
                name = match.group(1)
                parsed = Section(name=name)
            else:
                line_type = LineType.UNKNOWN
                parsed = None

    return Line(raw=raw, line_type=line_type, parsed=parsed)


if __name__ == "__main__":
    import sys
    import os
    args = sys.argv[1:]

    config_path = args[0] if len(args) > 0 else os.path.expanduser("~/.config/i3/config")
    max_lines = int(args[1]) if len(args) > 1 else 10000

    print(f"Parsing first {max_lines} lines of '{config_path}'")

    with open(config_path, "rb") as f:
        config_text = f.read().decode("utf-8")

    lines = config_text.split("\n")
    lines[:max_lines]

    for line_str in lines:
        line = parse_line(line_str)
        print(f"[{line.line_type}] {line.raw}")
        if line.parsed:
            print(line.parsed)

