# Wallpapers

These wallpapers are from my private repo `gitlab.com/my-linux-config/my-wallpapers`, which contains the high res originals.

## License

All wallpapers are photos taken by me.
They are released here under the terms of the [Creative Commons BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/).
