# Make firefox use wayland
export MOZ_ENABLE_WAYLAND=1

{% if is_vm %}
# Make sure that the cursor is visible
# Bug is only affecting VMs (https://github.com/swaywm/sway/issues/3814)
export WLR_NO_HARDWARE_CURSORS=1
{% endif %}

export PATH="$PATH:$HOME/.config/sway/bin"

# Not sure if it is required, but may be useful in the future? SEE https://wiki.archlinux.org/title/Xdg-utils
export XDG_CURRENT_DESKTOP=sway

# For fixing Java apps. SEE https://github.com/swaywm/sway/wiki#issues-with-java-applications
export _JAVA_AWT_WM_NONREPARENTING=1

