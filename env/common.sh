# Add the local scripts to the path (with low priority)
# Add scripts that are required for wmc to function
export PATH="$PATH:$HOME/.wmc/bin/"
# There directories do not necessarily exist, but will make it easy to have machine specific scripts (and stuff installed with pip)
export PATH="$PATH:$HOME/.local/bin"


# Make gpg pinentry work?
export GPG_TTY=$(tty)

# Choose your preferred editor
export EDITOR="${EDITOR:-nano}"
