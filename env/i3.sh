export PATH="$PATH:$HOME/.config/i3/bin"

# Not sure if it is required, but may be useful in the future? SEE https://wiki.archlinux.org/title/Xdg-utils
# Also can help my own script determine, how they should behave
export XDG_CURRENT_DESKTOP=i3
