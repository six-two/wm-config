# My WM config

The config files for the sway/i3 window manager.
Also contains the configs for macOS tiling managers I use: Currently only [AeroSpace](https://github.com/nikitabobko/AeroSpace) maybe [Amethyst](https://github.com/ianyh/Amethyst), depending on which works better.

## AeroSpace

### Installation

Just symlink the config file in this repository to one of the default locations:
```bash
test -f aerospace.toml && ln -s "$PWD/aerospace.toml" ~/.aerospace.toml
```

## i3 / sway

### TODOS

 - Bring sway config up to speed (lacks some features like typing, etc)
 - Make is it simple to have per profile keybindings in a config file
 - Implement type-/valuechecking variables from config files
 - Handle nested app definitions somehow
 - IDEA: Paint the current date on the background picture (like conky). Will require regular refreshs of the image.
 - Rewrite the whole thing in rust -> cleaner structure/design + I learn Rust :)

### Installation

Clone this repo to the correct place:
```
git clone https://gitlab.com/six-two/wm-config $HOME/.wmc
```

Source the WMC environment:
```
source $HOME/.wmc/env/common.sh
```

#### Dependencies
Install the wanted dependencies (see section below). For example:
```
sudo pacman -S i3status-rust i3 xorg-xinit xorg-server feh python python-pip
```

Install pip dependencies:
```
pip install -r $HOME/.wmc/dependencies/pip.txt
```

Check for missing dependencies:
```
wmc-missing-dependencies
```

Install any wanted dependencies you may have forgotten.

Build the config:
```bash
wmc-build
```


#### Post installation

Make sure everything runs correctly by running and selecting the WM you would like to use (you might want to try this in a different virtual terminal):
```
wmc-start
```

You can exit the WM by pressing `Win+Shift+Esc` to enter `System mode` and then pressing `E`.

If you want to autostart sway on `tty1` also append this to the same file (make it the last line, since it may use `exec`)
```
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  exec $HOME/.wmc/bin/wmc-start sway
fi
```

#### Updating

Run `wmc-update`

#### Overriding the config

To overwrite `<variable_name>` with the value `<value>` in `config/<file_path>.yaml`, create a file named `config/<file_path>-override.yaml`, and add the following line:

```
<variable_name>: <value>
```

Example:
To change the default modifier to `Mod1` (the `Alt` key), add `mod: Mod1` to `config/wmc-overrride.yaml`

### Dependencies

The following packages are required / recommended on Arch Linux.
On a different distro some packages may be named differently or may not be available.

#### Minimal dependencies

These packages are required or strongly recomended.

```
sudo pacman -S --needed bash git python python-pip sway xorg-xwayland
pip install networkx pyyaml munch python-liquid
```

Package | Reason
---|---
bash | Used for scripts
git | Downloading and updating this repo
python | Python3, used for scripts
python-pip | Python packages
sway | The window manager
xorg-xwayland | Run apps that don't support wayland

#### Feature dependencies

*TODO this is outdated*

These packages add extra features, but it will still work without it.
You might need to make small changes to the configs (especially `template/vars.conf`).

```
sudo pacman -S --needed alacritty brightnessctl dmenu grim i3status-rust libnotify mako pulseaudio swayidle swaylock systemd wtype
pikaur -S networkmanager-dmenu-git
```

Package | Reason
---|---
?alsa, pulseaudio-alsa | sound
alacritty | Terminal emulator
alsa-utils | `amixer` for sound in statusbar
brightnessctl | change screen brightness
dmenu | for networkmanager-dmenu-git
grim | Screenshots
i3status-rust | content for statusbar
libnotify | `notify-send` for notifications
mako | display notifications
networkmanager-dmenu-git | [aur] manage networks (WLAN, Ethernet)
pulseaudio | `pactl` for volume/mute keys
swayidle | Lockscreen
swaylock | Lockscreen
systemd | `loginctl` for lockscreen, `systemctl` for shutdowns, hibernation, etc
wtype | Special characters (like german umlauts)
xhost | You might need this for running X apps as root (like `sudo gparted`)

#### i3
If you want to also install i3 (in case you need apps like zoom, that don't work under xwayland) you will want to install these packages too.

```
sudo pacman -S --needed feh i3 xorg-server xorg-xinit
pikaur -S twmn-git
```

Package | Reason
---|---
feh | Background image
i3lock | Locking screen
i3 (group) | Window manager and some useful programs
twmn-git | Notification deamon. You can also use alternatives like dunst, etc
xorg-server | X server
xorg-xinit | for `startx`


#### Optional

These packages are pretty much a matter of personal choice and should be relatively easy to change or remove.
You may have to run something like `gpg --recv-key F4B432D5D67990E3` if you want to install wob.
```
sudo pacman -S --needed arch-audit firefox fish light pamixer playerctl thunar ttf-font-awesome
pikaur -S sgtk-menu sway-launcher-desktop wob
```

Package | Reason
---|---
arch-audit | list vulnerable installed packages
firefox | web browser
fish | nicer than `bash`
light | brightness bar
pamixer | volume bar
pikaur | [aur] aur package manager
playerctl | song playback control
sgtk-menu | [aur] graphical app launcher
sway-launcher-desktop | [aur] terminal based app launcher
thunar | file manager
ttf-font-awesome | Icons in statusbar
wob | [aur] brightness / volume bar

### Internal tasks

#### Update pinned python dependencies

First ensure that you have a recent version of `pip-tools` installed.
```bash
pip install -U pip-tools
```

Then generate/update the `requirements.txt` based in the `requirements.in`:
```bash
cd ~/.wmc/dependencies
pip-compile -U --allow-unsafe
```
