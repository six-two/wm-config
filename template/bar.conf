set $black          {{ theme.black | require }}
set $white          {{ theme.white | require }}
set $primary_light  {{ theme.primary_light | require }}
set $primary_dark   {{ theme.primary_dark | require }}
set $inactive_light {{ theme.inactive_light | require }}
set $inactive_dark  {{ theme.inactive_dark | require }}
set $urgent_light   {{ theme.urgent_light | require }}
set $urgent_dark    {{ theme.urgent_dark | require }}

font pango:DejaVu Sans Mono, FontAwesome 10

# border:       border around the title bar
# background:   title bar background
# text:         title bar text color
# indicator:    indicator whether split horizontal or vertical
# child_border: borders bottom, left, right around the window

# class                 border          background      text         indicator       child_border
# A client which currently has the focus.
client.focused          $primary_dark   $primary_dark   $white       $primary_light  $primary_dark
# A client which is the focused one of its container, but it does not have the focus at the moment.
client.focused_inactive $inactive_light $inactive_light $white       $inactive_light $inactive_light
# A client which is not the focused one of its container.
client.unfocused        $inactive_dark  $inactive_dark  $white       $inactive_dark  $inactive_dark
# A client which has its urgency hint activated.
client.urgent           $urgent_light   $urgent_light   $white       $urgent_light   $urgent_light
# Background and text color are used to draw placeholder window contents (when restoring layouts). Border and indicator are ignored.
client.placeholder      $black          $black          $white       $black          $black
# Background color which will be used to paint the background of the client window on top of which the client will be rendered. Only clients which do not cover the whole area of this window expose the color. Note that this colorclass only takes a single color.
client.background       $black

bar {
    font pango:DejaVu Sans Mono, FontAwesome 10
    strip_workspace_numbers yes

    {{host}} position bottom
    {{vm}}   position top

    colors {
        # Text color to be used for the statusline
        statusline $black
        # Background color of the bar.
        background $inactive_light
        # Text color to be used for the separator
        separator  $black

        # Text color to be used for the statusline on the currently focused monitor output. If not used, the color will be taken from statusline
        focused_statusline $white
        # Background color of the bar on the currently focused monitor output. If not used, the color will be taken from background
        focused_background $primary_dark
        # Text color to be used for the separator on the currently focused monitor output. If not used, the color will be taken from separator
        focused_separator  $white

        # <colorclass>     <border>       <background>   <text>
        focused_workspace  $primary_light $primary_light $black
        active_workspace   $primary_dark  $primary_dark  $white
        inactive_workspace $inactive_dark $inactive_dark $white
        urgent_workspace   $urgent_dark   $urgent_dark   $white
        binding_mode       $urgent_dark   $urgent_dark   $white
    }

    status_command {{ apps.statusbar | require }}
}
