############################ Merge conflict free section ###########################
# You can put custom stuff between the START and the END lines below.              #
# I will try to not change this section, so that there will be no merge conflicts. #
###################################### START #######################################
######################################  END  #######################################

# move focus to the parent container
bindsym {{mod}}+A focus parent
# Make the current focus fullscreen
bindsym {{mod}}+Shift+A fullscreen

# kill focused window
bindsym {{mod}}+Escape kill

# Move focus around
{% for direction in directions %}
    bindsym {{ mod }}+@{{ direction | upcase }} focus {{ direction }}
{% endfor %}

# _move_ the focused window with the same, but add Shift
{% for direction in directions %}
    bindsym {{ mod }}+Shift+@{{ direction | upcase }} move {{ direction }}
{% endfor %}

## Use {{mod}}+Ctrl+direction for window resizing: up/right make them bigger, down/left make them smaller
bindsym {{mod}}+Ctrl+@LEFT resize shrink width {{ config.small_resize | default: "25px" }}
bindsym {{mod}}+Ctrl+@RIGHT resize grow width {{ config.small_resize | default: "25px" }}
bindsym {{mod}}+Ctrl+@DOWN resize shrink height {{ config.small_resize | default: "25px" }}
bindsym {{mod}}+Ctrl+@UP resize grow height {{ config.small_resize | default: "25px" }}

# Switch tiling / floating mode
bindsym {{mod}}+Shift+F floating toggle
bindsym {{mod}}+F focus mode_toggle

# Toggle (vertical/horizontal): next window / current layout
bindsym {{mod}}+Shift+V layout toggle split
bindsym {{mod}}+V split toggle

# Scratchpad:
bindsym {{mod}}+Shift+minus move scratchpad
bindsym {{mod}}+minus scratchpad show

# Circle through workspaces
bindsym {{mod}}+bracketleft workspace prev_on_output
bindsym {{mod}}+bracketright workspace next_on_output
bindsym {{mod}}+Shift+bracketleft workspace prev
bindsym {{mod}}+Shift+bracketright workspace next
