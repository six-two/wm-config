# My keymap
My keymap is based on the standard US keyboard layout (I think).
But I have remapped Caps to Escape (using `setxkbmap -option caps:escape`).

```
,----,  ,---,---,---,---,---,---,---,---,---,---,---,---,
| Esc|  | F1| F2| F3| F4| F5| F6| F7| F8| F9|F10|F11|F12|
'----'  '---'---'---'---'---'---'---'---'---'---'---'---'
,---,---,---,---,---,---,---,---,---,---,---,---,---,-------,
| ` | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 | - | = | Backsp|
|---'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-----|
| Tab | Q | W | E | R | T | Y | U | I | O | P | [ | ] |  \  |
|-----',--',--',--',--',--',--',--',--',--',--',--',--'-----|
| Esc  | A | S | D | F | G | H | J | K | L | ; | ' | Enter  |
|------'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'-,-'--------|      ,----,     
| Shift  | Z | X | C | V | B | N | M | , | . | / |   Shift  |      | Up |     
|----,---',--'--,'---'---'---'---'---'-,-'---+---',----,----| ,----+----+----,
|Ctrl|Meta| Alt |           Space      |AltGr|Meta|Menu|Ctrl| |Left|Down|Rig.|
'----'----'-----'----------------------'-----'----'----'----' '----'----'----'
```

<!-- <style>h3 {text-align: center;}</style> -->

## Default mode
Any of the keys need to be prefixed with the modifier

### Function key row
Key(s) | Use | Use with shift
---|---|---
`Esc` | Kill container | System mode
`F1` - `F12` | Unused | Unused

### Number row
Key(s) | Use | Use with shift
---|---|---
`1` - `0` | Go to workspace | Move container to workspace
`-` | Show scratchpad | Move to scratchpad

### Top row
Key(s) | Use | Use with shift
---|---|---
`Tab` | Focus next output | Move workspace to next output
`Q` - `P` | Go to workspace | Move container to workspace
`[`, `]` | Go to prex/next workspace on output | Go to prex/next workspace

### Home row
Key(s) | Use | Use with shift
---|---|---
`Esc` (labeled `Caps`) | See `Esc` | See `Esc`
`A` | Focus parent | Fullscreen
`S` | App launcher mode (single) | App launcher mode (multi)
`D` | Launch app (dmenu) | Launch app (GUI)
`F` | Switch focus (tiling/floating mode) | Toggle window (tiling/floating mode)
`G` | Screenshot | Screenshot?
`H` - `L` | Move focus in direction (vim) | Move container in direction (vim)

### Bottom row
Key(s) | Use | Use with shift
---|---|---
`Z`, `X` | (Host) Go to workspace | (Host) Move container to workspace
`V` | Toggle next window (horizontal/vertical) | Toggle layout (horizontal/vertical)

### Modifier row
Key(s) | Use | Use with shift
---|---|---
`Space` | Terminal | Terminal (fallback)

### Direction keys (vim like)
`Left` | Alias for `H`
`Down` | Alias for `J`
`Up` | Alias for `K`
`Right` | Alias for `L`

### Mouse
Keys + Buttons | Use
---|---
`Mod`+`LeftClick` | Fullscreen
`Mod`+`RightClick` | Move away (to workspace `0`)
`Mod`+`MiddleClick` | Kill
`Mod`+`ScrollUp` | Next workspace on display
`Mod`+`ScrollDown` | Prev workspace on display
`Button8` (Back/Down) | Open GUI launcher
